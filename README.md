# Unreal Multiplayer Platformer


## Description

A small networked co-op puzzle platformer prototype made in [Unreal Engine 5](https://www.unrealengine.com/en-US/unreal-engine-5). 

This protoype contains pressure plates, movable platforms and doors and collectable keys which are replicated across clients.

Create and join online gameplay sessions utilising the [Online Subsystem Steam](https://docs.unrealengine.com/5.0/en-US/online-subsystem-steam-interface-in-unreal-engine/) to connect across the internet on Steam's development server.

You must solve the puzzles and collect all the keys to unlock the door to the pyramid and win the game.

![Screenshot__13_](/uploads/a9eb367deaeae223224571da483e4260/Screenshot__13_.png)

![Screenshot__9_](/uploads/2225bb12c19260d3b03f736eb5e36d0c/Screenshot__9_.png)

![Screenshot__15_](/uploads/e0b2064ef07af0005ea29677ee9d5678/Screenshot__15_.png)

![Screenshot__12_](/uploads/b02773d139c876a54be6c094808965c8/Screenshot__12_.png)

## To play

On windows you can extract the build from `export/windows.zip` and run the executable.

Or you can use unreal engine 5 and import the project and run it from in the engine.

***

## Authors and acknowledgment
Cael Rowley

## License
MIT

<b>NOTICE</b>: Game assets are not included under this license and have their own licensing. They are taken from the [stylized egypt](https://unrealengine.com/marketplace/en-US/product/stylized-egypt) asset pack from the unreal marketplace.
